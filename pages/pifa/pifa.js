// pages/pifa/pifa.js
const util = require('../../utils/util.js')
const BL = require('../../utils/lanya.js')
var pos = require('../../utils/pos.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    BL.chonglian(function (res) {
      if (res == "ok") {
        console.log("打印机已就续可以打印!")
      }
      else {
        //这里不用提示
        console.log(res)
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  pt:function(){
    BL.chonglian(function (res) {
      if (res == "ok") {
        console.log("可以开始打印了!")
        printText();
      }
      else {
        

        // 返回 A 正在连接 ，提示正在尝试连接，请稍后再试  B 出错  提示出错，请检查打印机，或重新设置
        wx.showToast({
          title: res,
          icon: 'success',
          duration: 2000
        })
      }
    });
   
  }
})
function printText() {
  console.log("开始打印")
  //pos.PrintText("情景4:综合支付不使用零钱也不存零,无找零");//打印标题
  pos.PrintJumpLines(1);
  pos.PrintTitle(" 超市名(可自定义)");//打印标题
  pos.PrintNameValue('单号: ', 'ls0220180125000001');
  pos.PrintText("收银员: 1001    机器编号: 001");//打印标题
  pos.PrintNameValue('时间: ', '2017-12-23 12:01:01');
  pos.PrintText("品名   数量    单价         小计");
  pos.PrintMiddleText('--------------------------------');
  // pos.PrintText("可口可乐(听)");
  // pos.PPP("2.0", "2", "4.00");
  // pos.PrintText("大泽山葡萄(kg)");
  // pos.PPP("12.8", "2.0", "25.6");
  // pos.PrintText("大泽山葡萄(盒)");
  // pos.PPP("30", "2.0", "60.0");
  // pos.PrintMiddleText('--------------------------------');
  // pos.PrintNameValue('合计数量: ', '6.0');
  // pos.PrintNameValue('标价金额: ', '128.8');
  // pos.PrintText('合计优惠(促销/特价等):  -2.3');
  // pos.PrintNameValue('合计应收: ', '126.5');
  // pos.PrintNameValue('实际收款: ', '126.5');
  // //pos.PrintNameValue('优惠(抹零): ', '0.5');
  // pos.PrintMiddleText('--------------------------------');
  // pos.PrintText("收款明细: ");//打印标题
  // pos.PrintNameValue('现金: ', '100');
  // pos.PrintNameValue('微信刷卡: ', '26.5');
  // //pos.PrintNameValue('零钱包支付: ', '0.5');
  // //pos.PrintNameValue('现金找零: ', '4.0');
  // pos.PrintMiddleText('--------------------------------');
  // pos.PrintNameValue('会员卡: ', '132****8588');
  // //pos.PrintNameValue('支付前余额: ', '5.00');
  // pos.PrintNameValue('当前可用余额: ', '4.5');
  // pos.PrintNameValue('本次积分: ', '12.0');
  // pos.PrintNameValue('当前可用积分: ', '285.0');
  // pos.PrintMiddleText('--------------------------------');
  // pos.PrintText("谢谢您的惠顾,欢迎下次光临!");//打印标题
  // pos.PrintQRcode('http://xxx.xxx.xxx');//二维码  PrintBarcode: PrintBarcode,//打印条码
  // pos.PrintJumpLines(3);
  // pos.PrintBarcode('6934199500017');//条码
  pos.PrintJumpLines(3);
  pos.DaYin(function(res){
      if(res=="err")
      {
        pos.ClearQueue();//清空打印列队
        //提示,打印出错,请稍后再试,仍然出错请重新设置打印机
      }
  });
}